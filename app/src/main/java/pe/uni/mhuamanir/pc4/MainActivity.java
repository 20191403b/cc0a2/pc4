package pe.uni.mhuamanir.pc4;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    GridView gridView;
    ArrayList<String> textFood=new ArrayList<>();
    ArrayList<Integer> imageFood=new ArrayList<>();
    ArrayList<String> DescriptionFood=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        gridView=findViewById(R.id.grid);
        fillArray();
        GridAdapter gridAdapter=new GridAdapter(this,textFood,imageFood,DescriptionFood);
        gridView.setAdapter(gridAdapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent=new Intent(MainActivity.this,FormularioComidaActivity.class);
                intent.putExtra("FOOD",textFood.get(position));
                startActivity(intent);
            }
        });

    }

    private void fillArray(){
        textFood.add("Pollo a la brasa");
        textFood.add("amburguesa");
        textFood.add("ceviche");
        textFood.add("pizza");
        textFood.add("ají de gallina");
        textFood.add("causa rellena");
        textFood.add("caldo de gallina");
        textFood.add("juane");

        imageFood.add(R.drawable.pollo_brasa);
        imageFood.add(R.drawable.amburguesa);
        imageFood.add(R.drawable.ceviche);
        imageFood.add(R.drawable.pizza);
        imageFood.add(R.drawable.aji_gallina);
        imageFood.add(R.drawable.causa);
        imageFood.add(R.drawable.caldo);
        imageFood.add(R.drawable.juane);

        DescriptionFood.add("Pollo a la brasa entero con papas");
        DescriptionFood.add("amburguesa con todas las cremas");
        DescriptionFood.add("ceviche con aji");
        DescriptionFood.add("pizza mericana");
        DescriptionFood.add("ají de gallina y huevo");
        DescriptionFood.add("causa rellena ");
        DescriptionFood.add("caldo de gallina");
        DescriptionFood.add("juane");
    }
}