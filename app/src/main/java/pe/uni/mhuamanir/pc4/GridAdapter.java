package pe.uni.mhuamanir.pc4;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class GridAdapter extends BaseAdapter {
    Context context;
    ArrayList<String> textFood=new ArrayList<>();
    ArrayList<Integer> imageFood=new ArrayList<>();
    ArrayList<String> descriptionFood=new ArrayList<>();

    public GridAdapter(Context context, ArrayList<String> textFood, ArrayList<Integer> imageFood, ArrayList<String> descriptionFood) {
        this.context = context;
        this.textFood = textFood;
        this.imageFood = imageFood;
        this.descriptionFood = descriptionFood;
    }

    @Override
    public int getCount() {
        return textFood.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }
    @SuppressLint("ViewHolder")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_view,parent,false);
        ImageView imageView = view.findViewById(R.id.image_view_food);
        TextView tittleFood = view.findViewById(R.id.tittle_food);
        TextView descriptionTextFood = view.findViewById(R.id.description_food);

        imageView.setImageResource(imageFood.get(position));
        tittleFood.setText(textFood.get(position));
        descriptionTextFood.setText(descriptionFood.get(position));
        return view;
    }
}
